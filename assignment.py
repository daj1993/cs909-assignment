import nltk
import sklearn
import lda
import csv
import copy
import itertools
import random
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.grid_search import GridSearchCV


header = []
corpus = []
popular_topics = ['topic.earn', 'topic.acq', 'topic.money.fx', 'topic.grain', 'topic.crude', 'topic.trade', 'topic.interest', 'topic.ship', 'topic.wheat',	'topic.corn']

# translates POS symbols from one type to another, used for named entity recognition
def posConvertor(tag):
	if tag == 'NN' or tag == 'NNS' or tag == 'NNP' or tag == 'NNPS':
		return nltk.corpus.wordnet.NOUN
	if tag == 'RB' or tag == 'RBS' or tag == 'RP':
		return nltk.corpus.wordnet.ADV
	if tag == 'VB' or tag == 'VBD' or tag == 'VBG' or tag == 'VBN' or tag == 'VBP' or tag == 'VBZ':
		return nltk.corpus.wordnet.VERB
	if tag == 'JJ' or tag == 'JJR' or tag == 'JJS':
		return nltk.corpus.wordnet.ADJ
	return None

# finds the names of the different topics
def header_vocab(header):
	vocab = []
	for h in header:
		vocab.append(h[6:].replace('.', ' '))
	return vocab

# splits the whole reuters dataset into training and testing data
def pre_process(header,corpus):
	popular_topic_index = []

	for h in range(0,len(header)):
		if header[h] in popular_topics:
			popular_topic_index.append(h)
	
	train_data = []
	test_data = []

	for l in corpus:
		for i in popular_topic_index:
			if l[2] == 'train':
				if int(l[i]) > 0:
					train_data.append(l)
					break
			elif l[2] == 'test':
				if int(l[i]) > 0:
					test_data.append(l)
					break
					
	return train_data, test_data

# performs the actual pre-processing pipeline, using NLTK
def simple_features(document_text):
	# remove punctuation
	tokenizer = nltk.tokenize.RegexpTokenizer(r'\w+')
	# split document into tokens
	tokens = tokenizer.tokenize(document_text)
	# pos tagging
	# remove erroneous token
	if tokens.count('\xc0_') > 0:
		tokens.remove('\xc0_')
	tagged = nltk.pos_tag(tokens)
	# create lemmatizer
	wordnet_lemmatizer = nltk.stem.WordNetLemmatizer()
	lemmatized = []
	for t in tagged:
		pos = posConvertor(t[1])
		if pos != None:
			lemmatized.append((wordnet_lemmatizer.lemmatize(t[0], pos), t[1]))
		else:
			lemmatized.append(t)
	exclude_stopwords = [w for w in lemmatized if not w[0] in nltk.corpus.stopwords.words('english')]
	
	entities = nltk.chunk.ne_chunk(exclude_stopwords)
	entities_list = nltk.chunk.util.tree2conlltags(entities)

	words = ''
	for e in entities_list:
		if e[1] == 'CD':
			words += 'NUM'
		elif e[2] != 'O':
			words += e[2].encode('ascii')
		else:
			words += e[0].encode('ascii')
		words += ' '
	return words[:-1]
	
# takes instances with multiple topics and creates a new instance for each individual topic
def expand(data):
	popular_topic_index = []

	for h in range(0,len(header)):
		if header[h] in popular_topics:
			popular_topic_index.append(h)
			
	new_data = []
	
	for d in data:
		for i in popular_topic_index:
			if int(d[i]) > 0:
				new_data.append([d[0], d[138], d[139], header[i]])
				
	return new_data

# represents unigrams of the training data using a frequency vectorizer
def count_vectorize_features(train_features, test_features, max):
	vectorizer = sklearn.feature_extraction.text.CountVectorizer(analyzer = "word", tokenizer = None, preprocessor = None, stop_words = None, max_features=max)
	vectorizer.fit(train_features)
	train = vectorizer.transform(train_features)
	test = vectorizer.transform(test_features)
	return vectorizer, train, test

# represents bigrams of the training data using a frequency vectorizer
def count_vectorize_bigrams(train_features, test_features, max):
	vectorizer = sklearn.feature_extraction.text.CountVectorizer(analyzer = "word", tokenizer = None, preprocessor = None, stop_words = None, max_features=max, ngram_range=(2,2))
	vectorizer.fit(train_features)
	train = vectorizer.transform(train_features)
	test = vectorizer.transform(test_features)
	return vectorizer, train, test

# represents topics of the training data using a frequency vectorizer
def count_vectorize_topics(train_features, test_features, topics):
	vectorizer = sklearn.feature_extraction.text.CountVectorizer(analyzer = "word", tokenizer = None, preprocessor = None, stop_words = None, vocabulary=topics)
	vectorizer.fit(train_features)
	train = vectorizer.transform(train_features)
	test = vectorizer.transform(test_features)
	return vectorizer, train, test

# represents unigrams of the training data using a tfidf vectorizer
def tfidf_vectorize_features(train_features, test_features, max):
	vectorizer = sklearn.feature_extraction.text.TfidfVectorizer(analyzer = "word", tokenizer = None, preprocessor = None, stop_words = None, max_features=max)
	vectorizer.fit(train_features)
	train = vectorizer.transform(train_features)
	test = vectorizer.transform(test_features)
	return vectorizer, train, test
	
# represents bigrams of the training data using a tfidf vectorizer
def tfidf_vectorize_bigrams(train_features, test_features, max):
	vectorizer = sklearn.feature_extraction.text.TfidfVectorizer(analyzer = "word", tokenizer = None, preprocessor = None, stop_words = None, max_features=max, ngram_range=(2,2))
	vectorizer.fit(train_features)
	train = vectorizer.transform(train_features)
	test = vectorizer.transform(test_features)
	return vectorizer, train, test

# represents topics of the training data using the tfidf vectorizer
def tfidf_vectorize_topics(train_features, test_features, topics):
	vectorizer = sklearn.feature_extraction.text.TfidfVectorizer(analyzer = "word", tokenizer = None, preprocessor = None, stop_words = None, vocabulary=topics)
	vectorizer.fit(train_features)
	train = vectorizer.transform(train_features)
	test = vectorizer.transform(test_features)
	return vectorizer, train, test

# finds topic models within a feature space
def lda_topic_models(vectorizer, vocab, n_top_words):
	model = lda.LDA(n_topics=10, n_iter=500, random_state=1)
	model.fit(vocab)
	topic_word = model.topic_word_  # model.components_ also works

	topics = []
	for i, topic_dist in enumerate(topic_word):
		topics.append(np.array(vectorizer.get_feature_names())[np.argsort(topic_dist)][:-n_top_words:-1])
	
	return topics

# takes the different topics and creates a vocabulary of them, containing all the words 
def topics_to_vocab(topics):
	vocabulary = set()
	for t in topics:
		for i in t:
			vocabulary.add(i)
	return list(vocabulary)

# takes a list of sentences and converts them to a set of words, the vocabulary
def create_vocab_list(features):
	full_vocab = set()
	for f in features:
		# remove punctuation
		tokenizer = nltk.tokenize.RegexpTokenizer(r'\w+')
		# split document into tokens
		tokens = tokenizer.tokenize(f)
		for t in tokens:
			full_vocab.add(t)
	return list(full_vocab)
	
# performs cross validation on some training data, returning the performances across each fold
def cross_validation(classifier_method, data, classes, length):
	classifier = classifier_method.fit(data, classes)
	
	return classifier, sklearn.cross_validation.cross_val_score(classifier, data, classes, cv=10)
	
# reads in the original reuters dataset and shuffles it with a seed value
def read_full_dataset(filename):
	global header
	global corpus
	last_line = None
	with open(filename, 'rb') as csvfile:
		csvreader = csv.reader(csvfile, delimiter=',')
		for line in csvreader:
			if not last_line == None:
				corpus.append(list(line))
			else:
				header = list(line)
			last_line = line
	train_data, test_data = pre_process(header,corpus)

	train_data = expand(train_data)
	test_data = expand(test_data)

	random.seed(42)
	random.shuffle(train_data)
	
	return train_data, test_data

# reads a csv file into a list of lists
def read_csv(filename):
	data = []
	with open(filename, 'rb') as csvfile:
		csvreader = csv.reader(csvfile, delimiter=',')
		for line in csvreader:
			data.append(list(line))
	return data

# splits a list of lists into two lists, one of features and one of classes	
def seperate_features_and_classes(data):
	features = []
	classes = []
	for d in data:
		features.append(d[2])
		classes.append(d[3])
	return features, classes

# brute force to find the optimal number of features to provide strong classifier accuracy, for unigrams
def find_optimal_num_features(classifier_method):
	performances = []
	indices = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000]
	for i in indices:
		print 'processing ' + str(i)
		vec, train_vec_data, test_vec_data = count_vectorize_features(train_features, test_features, i)
		classifier, performance = cross_validation(classifier_method, train_vec_data, train_classes, len(training_data))
		performances.append(reduce(lambda x, y: x + y, performance) / len(performance))
		
		
	fig,ax = plt.subplots()
	ax.plot(indices, performances)
	ax.set_xlabel('Number of Features')
	ax.set_ylabel('Accuracy')
	fig.show()
	plt.savefig(classifier_method.__class__.__name__ + '-feature-selection.jpg')

# brute force to find the optimal number of features to provide strong classifier accuracy, for bigrams
def find_optimal_num_bigram_features(classifier_method):
	performances = []
	indices = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000]
	for i in indices:
		print 'processing ' + str(i)
		vec, train_vec_data, test_vec_data = count_vectorize_bigrams(train_features, test_features, i)
		classifier, performance = cross_validation(classifier_method, train_vec_data, train_classes, len(training_data))
		performances.append(reduce(lambda x, y: x + y, performance) / len(performance))
		
		
	fig,ax = plt.subplots()
	ax.plot(indices, performances)
	ax.set_xlabel('Number of Features')
	ax.set_ylabel('Accuracy')
	fig.show()
	plt.savefig(classifier_method.__class__.__name__ + '-bigrams-selection.jpg')
	
# brute force to find the optimal number of features to provide strong classifier accuracy, for topics
def find_optimal_num_topics(classifier_method):
	performances = []
	indices = [51, 101, 201, 301, 401, 501]
	for i in indices:
		print 'processing ' + str(i)
		vec, train_vec_data, test_vec_data = count_vectorize_features(train_features, test_features, None)
		topics = lda_topic_models(vec, train_vec_data, i)
		print topics
		vec, train_vec_data, test_vec_data = count_vectorize_topics(train_features, test_features, topics_to_vocab(topics))
		classifier, performance = cross_validation(classifier_method, train_vec_data, train_classes, len(training_data))
		performances.append(reduce(lambda x, y: x + y, performance) / len(performance))
		
		
	fig,ax = plt.subplots()
	ax.plot(indices, performances)
	ax.set_xlabel('Number of Topics')
	ax.set_ylabel('Accuracy')
	fig.show()
	plt.savefig(classifier_method.__class__.__name__ + '-topic-selection.jpg')

# evaluate the performance of a classifier using a count vectorizer and unigrams
def eval_using_count_vec(classifier_method, tuning_params, max_features):
	clf = GridSearchCV(classifier_method, tuning_params, cv=10, verbose=0)
	vec, train_vec_data, test_vec_data = count_vectorize_features(train_features, test_features, max_features)
	clf.fit(train_vec_data, train_classes)
	classifier_method.set_params(**clf.best_params_)

	classifier, performance = cross_validation(classifier_method, train_vec_data, train_classes, len(training_data))
	print classifier_method.__class__.__name__
	print clf.best_params_
	print performance
	train_predicted_class = classifier.predict(train_vec_data)
	test_predicted_class = classifier.predict(test_vec_data)
	print sklearn.metrics.accuracy_score(test_classes, test_predicted_class)
	print sklearn.metrics.precision_recall_fscore_support(test_classes, test_predicted_class, average='macro')
	print sklearn.metrics.precision_recall_fscore_support(train_classes, train_predicted_class, average='macro')
	print sklearn.metrics.precision_recall_fscore_support(train_classes, train_predicted_class, average='micro')
	cm = sklearn.metrics.confusion_matrix(train_classes, train_predicted_class)
	print cm
	print_confusion_matrix(cm, classifier_method.__class__.__name__ + '-count_vec.jpg')

# evaluate the performance of a classifier using a count vectorizer and bigrams
def eval_using_count_bigrams(classifier_method, tuning_params, max_features):
	clf = GridSearchCV(classifier_method, tuning_params, cv=10, verbose=0)
	vec, train_vec_data, test_vec_data = count_vectorize_bigrams(train_features, test_features, max_features)
	clf.fit(train_vec_data, train_classes)
	classifier_method.set_params(**clf.best_params_)
	
	classifier, performance = cross_validation(classifier_method, train_vec_data, train_classes, len(training_data))
	print classifier_method.__class__.__name__
	print clf.best_params_
	print performance
	train_predicted_class = classifier.predict(train_vec_data)
	test_predicted_class = classifier.predict(test_vec_data)
	print sklearn.metrics.accuracy_score(test_classes, test_predicted_class)
	print sklearn.metrics.precision_recall_fscore_support(test_classes, test_predicted_class, average='macro')
	print sklearn.metrics.precision_recall_fscore_support(train_classes, train_predicted_class, average='macro')
	print sklearn.metrics.precision_recall_fscore_support(train_classes, train_predicted_class, average='micro')
	cm = sklearn.metrics.confusion_matrix(train_classes, train_predicted_class)
	print cm
	print_confusion_matrix(cm, classifier_method.__class__.__name__ + '-count_bigrams.jpg')

# evaluate the performance of a classifier using a tfidf vectorizer and unigrams
def eval_using_tfidf_vec(classifier_method, tuning_params, max_features):
	clf = GridSearchCV(classifier_method, tuning_params, cv=10, verbose=0)
	vec, train_vec_data, test_vec_data = tfidf_vectorize_features(train_features, test_features, max_features)
	clf.fit(train_vec_data, train_classes)
	classifier_method.set_params(**clf.best_params_)
	
	classifier, performance = cross_validation(classifier_method, train_vec_data, train_classes, len(training_data))
	print classifier_method.__class__.__name__
	print clf.best_params_
	print performance
	train_predicted_class = classifier.predict(train_vec_data)
	test_predicted_class = classifier.predict(test_vec_data)
	print sklearn.metrics.accuracy_score(test_classes, test_predicted_class)
	print sklearn.metrics.precision_recall_fscore_support(test_classes, test_predicted_class, average='macro')
	print sklearn.metrics.precision_recall_fscore_support(train_classes, train_predicted_class, average='macro')
	print sklearn.metrics.precision_recall_fscore_support(train_classes, train_predicted_class, average='micro')
	cm = sklearn.metrics.confusion_matrix(train_classes, train_predicted_class)
	print cm
	print_confusion_matrix(cm, classifier_method.__class__.__name__ + '-tfidf_vec.jpg')
	
# evaluate the performance of a classifier using a tfidf vectorizer and bigrams
def eval_using_tfidf_bigrams(classifier_method, tuning_params, max_features):
	clf = GridSearchCV(classifier_method, tuning_params, cv=10, verbose=0)
	vec, train_vec_data, test_vec_data = tfidf_vectorize_bigrams(train_features, test_features, max_features)
	clf.fit(train_vec_data, train_classes)
	classifier_method.set_params(**clf.best_params_)
	
	classifier, performance = cross_validation(classifier_method, train_vec_data, train_classes, len(training_data))
	print classifier_method.__class__.__name__
	print clf.best_params_
	print performance
	train_predicted_class = classifier.predict(train_vec_data)
	test_predicted_class = classifier.predict(test_vec_data)
	print sklearn.metrics.accuracy_score(test_classes, test_predicted_class)
	print sklearn.metrics.precision_recall_fscore_support(test_classes, test_predicted_class, average='macro')
	print sklearn.metrics.precision_recall_fscore_support(train_classes, train_predicted_class, average='macro')
	print sklearn.metrics.precision_recall_fscore_support(train_classes, train_predicted_class, average='micro')
	cm = sklearn.metrics.confusion_matrix(train_classes, train_predicted_class)
	print cm
	print_confusion_matrix(cm, classifier_method.__class__.__name__ + '-tfidf_bigrams.jpg')

# evaluate the performance of a classifier using a count vectorizer and topics
def eval_using_count_lda_topics(classifier_method, tuning_params, topic_voc):
	clf = GridSearchCV(classifier_method, tuning_params, cv=10, verbose=0)
	vec, train_vec_data, test_vec_data = count_vectorize_topics(train_features, test_features, topic_voc)
	clf.fit(train_vec_data, train_classes)
	classifier_method.set_params(**clf.best_params_)
	
	classifier, performance = cross_validation(classifier_method, train_vec_data, train_classes, len(training_data))
	print classifier_method.__class__.__name__
	print clf.best_params_
	print performance
	train_predicted_class = classifier.predict(train_vec_data)
	test_predicted_class = classifier.predict(test_vec_data)
	print sklearn.metrics.accuracy_score(test_classes, test_predicted_class)
	print sklearn.metrics.precision_recall_fscore_support(test_classes, test_predicted_class, average='macro')
	print sklearn.metrics.precision_recall_fscore_support(train_classes, train_predicted_class, average='macro')
	print sklearn.metrics.precision_recall_fscore_support(train_classes, train_predicted_class, average='micro')
	cm = sklearn.metrics.confusion_matrix(train_classes, train_predicted_class)
	print cm
	print_confusion_matrix(cm, classifier_method.__class__.__name__ + '-count_lda_topics.jpg')

# evaluate the performance of a classifier using a tfidf vectorizer and topics
def eval_using_tfidf_lda_topics(classifier_method, tuning_params, topic_voc):
	clf = GridSearchCV(classifier_method, tuning_params, cv=10, verbose=0)
	vec, train_vec_data, test_vec_data = tfidf_vectorize_topics(train_features, test_features, topic_voc)
	clf.fit(train_vec_data, train_classes)
	classifier_method.set_params(**clf.best_params_)
	
	classifier, performance = cross_validation(classifier_method, train_vec_data, train_classes, len(training_data))
	print classifier_method.__class__.__name__
	print clf.best_params_
	print performance
	train_predicted_class = classifier.predict(train_vec_data)
	test_predicted_class = classifier.predict(test_vec_data)
	print sklearn.metrics.accuracy_score(test_classes, test_predicted_class)
	print sklearn.metrics.precision_recall_fscore_support(test_classes, test_predicted_class, average='macro')
	print sklearn.metrics.precision_recall_fscore_support(train_classes, train_predicted_class, average='macro')
	print sklearn.metrics.precision_recall_fscore_support(train_classes, train_predicted_class, average='micro')
	cm = sklearn.metrics.confusion_matrix(train_classes, train_predicted_class)
	print cm
	print_confusion_matrix(cm, classifier_method.__class__.__name__ + '-tfidf_lda_topics.jpg')

# stores a image of a confusion matrix in a file
def print_confusion_matrix(cm, save_file):
	cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
	plt.figure()
	plot_confusion_matrix(cm_normalized, title='')

	plt.savefig(save_file)

# plots a confusion matrix from a given confusion matrix
def plot_confusion_matrix(cm, title='', cmap=plt.cm.Blues):
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(popular_topics))
    plt.xticks(tick_marks, popular_topics, rotation=45)
    plt.yticks(tick_marks, popular_topics)
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

# reads in the training and test data
training_data = read_csv('trainData.csv')
train_features, train_classes = seperate_features_and_classes(training_data)
testing_data = read_csv('testData.csv')
test_features, test_classes = seperate_features_and_classes(testing_data)
# specify the maximum number of features to use for the representation
maximum_features = 300
maximum_topic_words = 300
maximum_bigram_features = 800

# create the topics to be used for the topic modelling
vec, train_v_d, test_v_d = count_vectorize_features(train_features, test_features, None)
topics = lda_topic_models(vec, train_v_d, maximum_topic_words)
vocab_for_topics = topics_to_vocab(topics)

# Set the parameters to be optimised during grid search cross-validation
svc_tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4], 'C': [1, 10, 100, 1000]},
						{'kernel': ['sigmoid'], 'gamma': [1e-3, 1e-4], 'C': [1, 10, 100, 1000], 'coef0': [0, 0.5, 1]},
						{'kernel': ['linear'], 'C': [1, 10, 100, 1000]}]
						
nb_tuned_parameters = [{'alpha': [0, 0.5, 1, 1.5, 2], 'fit_prior': [True, False]}]
rf_tuned_parameters = [{'n_estimators':[1, 10, 50, 100], 'max_features':['auto', 'log2']}]

# perform the actual evaluation on all models before printing the results to the screen
# 18 tests in total, 3 classifiers and 6 feature representations

eval_using_count_vec(MultinomialNB(), nb_tuned_parameters, maximum_features)
eval_using_count_vec(SVC(), svc_tuned_parameters, maximum_features)
eval_using_count_vec(RandomForestClassifier(), rf_tuned_parameters, maximum_features)

eval_using_count_bigrams(MultinomialNB(), nb_tuned_parameters, maximum_bigram_features)
eval_using_count_bigrams(SVC(), svc_tuned_parameters, maximum_bigram_features)
eval_using_count_bigrams(RandomForestClassifier(), rf_tuned_parameters, maximum_bigram_features)

eval_using_tfidf_vec(MultinomialNB(), nb_tuned_parameters, maximum_features)
eval_using_tfidf_vec(SVC(), svc_tuned_parameters, maximum_features)
eval_using_tfidf_vec(RandomForestClassifier(), rf_tuned_parameters, maximum_features)

eval_using_tfidf_bigrams(MultinomialNB(), nb_tuned_parameters, maximum_bigram_features)
eval_using_tfidf_bigrams(SVC(), svc_tuned_parameters, maximum_bigram_features)
eval_using_tfidf_bigrams(RandomForestClassifier(), rf_tuned_parameters, maximum_bigram_features)

eval_using_count_lda_topics(MultinomialNB(), nb_tuned_parameters, vocab_for_topics)
eval_using_count_lda_topics(SVC(), svc_tuned_parameters, vocab_for_topics)
eval_using_count_lda_topics(RandomForestClassifier(), rf_tuned_parameters, vocab_for_topics)

eval_using_tfidf_lda_topics(MultinomialNB(), nb_tuned_parameters, vocab_for_topics)
eval_using_tfidf_lda_topics(SVC(), svc_tuned_parameters, vocab_for_topics)
eval_using_tfidf_lda_topics(RandomForestClassifier(), rf_tuned_parameters, vocab_for_topics)