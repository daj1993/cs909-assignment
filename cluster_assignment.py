import nltk
import sklearn
import lda
import csv
import copy
import itertools
import random
import numpy as np
from sklearn.cluster import KMeans
from sklearn.cluster import AgglomerativeClustering
from sklearn.cluster import DBSCAN

# simple function to read in a cv file
def read_csv(filename):
	data = []
	with open(filename, 'rb') as csvfile:
		csvreader = csv.reader(csvfile, delimiter=',')
		for line in csvreader:
			data.append(list(line))
	return data

# split the csv file into the features of the document and the classes
def seperate_features_and_classes(data):
	features = []
	classes = []
	for d in data:
		features.append(d[2])
		classes.append(d[3])
	return features, classes
	
# use a count vectorizer on unigrams
def count_vectorize_features(features, max):
	vectorizer = sklearn.feature_extraction.text.CountVectorizer(analyzer = "word", tokenizer = None, preprocessor = None, stop_words = None, max_features=max)
	vectorizer.fit(features)
	train = vectorizer.transform(features)
	return vectorizer, train

# use a tfidf vectorizer on topic models
def tfidf_vectorize_topics(features, classes, topics):
	vectorizer = sklearn.feature_extraction.text.TfidfVectorizer(analyzer = "word", tokenizer = None, preprocessor = None, stop_words = None, vocabulary=topics)
	vectorizer.fit(features, classes)
	train = vectorizer.transform(features)
	return vectorizer, train

# find the an estimation of the different topics within the documents
def lda_topic_models(vectorizer, vocab, n_top_words):
	model = lda.LDA(n_topics=10, n_iter=500, random_state=1)
	model.fit(vocab)
	topic_word = model.topic_word_  # model.components_ also works

	topics = []
	for i, topic_dist in enumerate(topic_word):
		topics.append(np.array(vectorizer.get_feature_names())[np.argsort(topic_dist)][:-n_top_words:-1])
	
	return topics

# convert the set of topics into a list of vocab to be used for the feature space
def topics_to_vocab(topics):
	vocabulary = set()
	for t in topics:
		for i in t:
			vocabulary.add(i)
	return list(vocabulary)

# print some performance metrics on the performance of the clusters
def evaluate_cluster(classifier, data, labels_true):
	p_counter = 0
	n_counter = 0
	for l in classifier.labels_:
		if l == -1:
			n_counter = n_counter + 1
		else:
			p_counter = p_counter + 1
	print ("Assigned: " + str(p_counter))
	print ("Unassigned: " + str(n_counter))
	n_clusters_ = len(set(classifier.labels_)) - (1 if -1 in classifier.labels_ else 0)
	print('Estimated number of clusters: %d' % n_clusters_)
	print("Silhouette Coefficient: %0.4f" % sklearn.metrics.silhouette_score(data, classifier.labels_))
	print("Adjusted Mutual Information: %0.4f" % sklearn.metrics.adjusted_mutual_info_score(labels_true, classifier.labels_))
	print("Homogeneity: %0.4f" % sklearn.metrics.homogeneity_score(labels_true, classifier.labels_))
	print("Completeness: %0.4f" % sklearn.metrics.completeness_score(labels_true, classifier.labels_))

# load the seperate training and testing data
training_data = read_csv('trainData.csv')
train_features, train_classes = seperate_features_and_classes(training_data)
testing_data = read_csv('testData.csv')
test_features, test_classes = seperate_features_and_classes(testing_data)
# join the lists together, no need for training and testing set now
features = train_features + test_features
classes = train_classes + test_classes
data = training_data + testing_data

maximum_topic_words = 300

# load the topics in from a seperate file, previous created to record them
with open ('topics.csv', 'rb') as csvfile:
	csvreader = csv.reader(csvfile, delimiter=',')
	for line in csvreader:
		vocab_for_topics = list(line)

# create a tfidf representation of the topics
tfidf_vec, trained_vec_data = tfidf_vectorize_topics(features, classes, vocab_for_topics)

print 'KMeans'

# perform k-means clustering
kmeans = KMeans(n_clusters=10, random_state=42)
kmeans.fit(trained_vec_data, classes)

# check the performance of the clustering
evaluate_cluster(kmeans, trained_vec_data.toarray(), classes)

print 'AgglomerativeClustering'

# perform agglomerative clustering
ac = AgglomerativeClustering(n_clusters=10)
temp = ac.fit_predict(trained_vec_data.toarray(), classes)

# check the performance of agglomerative clustering
evaluate_cluster(ac, trained_vec_data.toarray(), classes)

print ''

# perform DBSCAN clustering 
db = DBSCAN(eps=0.5, min_samples=12)
temp = db.fit_predict(trained_vec_data.toarray(), classes)

print 'DBSCAN'
# evaluate the performance of the clustering algorithm
evaluate_cluster(db, trained_vec_data.toarray(), classes)